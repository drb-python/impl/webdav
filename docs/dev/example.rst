.. _example:

Example
=======

Connect to your server
----------------------
.. literalinclude:: example/connect.py
    :language: python


List the first children
-----------------------
.. literalinclude:: example/children.py
    :language: python

Get the size of a file
-----------------------
.. literalinclude:: example/size.py
    :language: python

Download a file
---------------
.. literalinclude:: example/download.py
    :language: python