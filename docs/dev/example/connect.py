from requests.auth import HTTPBasicAuth

from drb.drivers.webdav import DrbWebdavNode, CertAuth

# Anonymous connection
node = DrbWebdavNode(webdav_hostname="hostname")

# Basic Auth connection
node = DrbWebdavNode(webdav_hostname="hostname",
                     auth=HTTPBasicAuth(
                         'username',
                         'password'
                     )
                     )

# Certificate connection
node = DrbWebdavNode(webdav_hostname="hostname",
                     auth=CertAuth(
                         'username',
                         'password',
                         '/etc/ssl/certs/certificate.crt',
                         '/etc/ssl/private/certificate.key'
                     )
                     )
