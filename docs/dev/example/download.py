import io

from drb.drivers.webdav import DrbWebdavNode

node = DrbWebdavNode(webdav_hostname="hostname")

# Download all the file
with node['file.txt'].get_impl(io.BytesIO) as stream:
    stream.read().decode()

# Download only the five first byte of the file
with node['file.txt'].get_impl(io.BytesIO) as stream:
    stream.read(5).decode()
