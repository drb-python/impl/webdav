from drb.drivers.webdav import DrbWebdavNode

# Anonymous connection
node = DrbWebdavNode(webdav_hostname="hostname")

for child in node:
    print(child.name)

children = node['test.txt']
