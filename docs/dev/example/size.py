from drb.drivers.webdav import DrbWebdavNode

node = DrbWebdavNode(webdav_hostname="hostname")

# Get all the attributes of the file.txt
node['file.txt'].attribute_names()

# Get the size of the file.txt
node['file.txt'] @ 'size'
