===================
Data Request Broker
===================
---------------------------------
webdav driver for DRB
---------------------------------
Release v\ |version|.


.. image:: https://pepy.tech/badge/drb-driver-webdav/month
    :target: https://pepy.tech/project/drb-driver-webdav
    :alt: Requests Downloads Per Month Badge

.. image:: https://img.shields.io/pypi/l/drb-driver-webdav.svg
    :target: https://pypi.org/project/drb-driver-webdav/
    :alt: License Badge

.. image:: https://img.shields.io/pypi/wheel/drb-driver-webdav.svg
    :target: https://pypi.org/project/drb-driver-webdav/
    :alt: Wheel Support Badge

.. image:: https://img.shields.io/pypi/pyversions/drb-driver-webdav.svg
    :target: https://pypi.org/project/drb-driver-webdav/
    :alt: Python Version Support Badge

-------------------

This python module includes data model implementation of webdav protocol

User Guide
==========

.. toctree::
   :maxdepth: 2

   user/install


.. toctree::

   dev/api

Example
=======

.. toctree::
   :maxdepth: 2

   dev/example

Others
======
.. toctree::
   :maxdepth: 2

   user/limitation
