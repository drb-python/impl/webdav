import multiprocessing
import os

from wsgidav import util
from wsgidav.fs_dav_provider import FilesystemProvider
from wsgidav.wsgidav_app import WsgiDAVApp

# ==============================================================================
# run_wsgidav_server
# ==============================================================================


def run_wsgidav_server(with_auth, with_ssl, provider=None, **kwargs):
    """Start blocking WsgiDAV server (called as a separate process)."""

    package_path = os.path.abspath(
        os.path.join(os.path.dirname(__file__), "..")
    )

    share_path = os.path.join('tests')
    if not os.path.exists(share_path):
        os.mkdir(share_path)

    if provider is None:
        provider = FilesystemProvider(share_path)

    # config = DEFAULT_CONFIG.copy()
    # config.update({
    config = {
        "host": "127.0.0.1",
        "port": 8080,
        "provider_mapping": {"/": provider},
        # None: dc.simple_dc.SimpleDomainController(user_mapping)
        "http_authenticator": {"domain_controller": None},
        "simple_dc": {"user_mapping": {"*": True}},  # anonymous access
        "verbose": 1,
        "logging": {
            "enable_loggers": [],
            # "debug_methods": [],
        },
        "property_manager": True,  # True: use property_manager.PropertyManager
        "lock_storage": True,  # True: use LockManager()
    }

    if with_auth:
        config["http_authenticator"].update(
            {
                "accept_basic": True,
                "accept_digest": False,
                "default_to_digest": False
            }
        )
        config["simple_dc"].update(
            {
                "user_mapping": {
                    "*": {
                        "tester": {
                            "password": "secret",
                            "description": "",
                            "roles": [],
                        },
                        "tester2": {
                            "password": "secret2",
                            "description": "",
                            "roles": [],
                        },
                    }
                }
            }
        )

    if with_ssl:
        config.update(
            {
                "ssl_certificate": os.path.join(
                    package_path, "wsgidav/server/sample_bogo_server.crt"
                ),
                "ssl_private_key": os.path.join(
                    package_path, "wsgidav/server/sample_bogo_server.key"
                ),
                "ssl_certificate_chain": None,
                # "accept_digest": True,
                # "default_to_digest": True,
            }
        )

    # We want output captured for tests
    util.init_logging(config)

    # This event is .set() when server enters the request handler loop
    if kwargs.get("startup_event"):
        config["startup_event"] = kwargs["startup_event"]

    app = WsgiDAVApp(config)

    # from wsgidav.server.server_cli import _runBuiltIn
    # _runBuiltIn(app, config, None)
    from wsgidav.server.server_cli import _run_cheroot

    _run_cheroot(app, config, "cheroot")
    # blocking...


# ========================================================================
# WsgiDavTestServer
# ========================================================================


class WsgiDavTestServer:
    """Run wsgidav in a separate process."""

    def __init__(
        self, config=None,
            with_auth=False,
            with_ssl=False,
            provider=None,
            profile=False
    ):
        self.config = config
        self.with_auth = with_auth
        self.with_ssl = with_ssl
        self.provider = provider
        # self.start_delay = 2
        self.startup_event = multiprocessing.Event()
        self.startup_timeout = 5
        self.proc = None
        assert not profile, "Not yet implemented"

    def __enter__(self):
        self.start()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.stop()

    def __del__(self):
        try:
            self.stop()
        except Exception:
            pass

    def start(self):
        kwargs = {
            "with_auth": self.with_auth,
            "with_ssl": self.with_ssl,
            "provider": self.provider,
            "startup_event": self.startup_event,
            "startup_timeout": self.startup_timeout,
        }
        self.proc = multiprocessing.Process(
            target=run_wsgidav_server,
            kwargs=kwargs
        )
        self.proc.daemon = True
        self.proc.start()

        # time.sleep(self.start_delay)
        if not self.startup_event.wait(self.startup_timeout):
            raise RuntimeError(
                "WsgiDavTestServer start() timed out after {} seconds".format(
                    self.startup_timeout
                )
            )
        return self

    def stop(self):
        if self.proc:
            self.proc.terminate()
            self.proc.join()
            self.proc = None
